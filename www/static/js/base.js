$(function(){
 $('.slider').each(function(i,e){
  var $slider = $(e),
    $controller = $slider.parent().parent().find('.s-controllers'); 
  $slider.cycle({ 
    fx:'scrollLeft',
    timeout:( (i+1.5)*4000 ),
    pause:true, 
    manualTrump: false,
    prev:$controller.find('.prev'),
    next:$controller.find('.next')
  });
  if( $(e).data('slider') =='pause' ){
    $(e).cycle('pause');
  }
  });
  
   
  $('.a-print').click(function(e){ 
  e.preventDefault(); 
  window.print();
  });
  //Main Banner 
  /*startingSlide 
*/
  var $body = $("body");
  if($body.hasClass("m-home") ){
    var home = {
      main:$('.w-banner .sliders'),
      prev:$('.w-banner .s-controllers .prev'),
      next:$('.w-banner .s-controllers .next')
    }
    var opts ={ 
      fx:'scrollLeft', 
      timeout: 4000  ,
      pause:true, 
      manualTrump: false,
      prev:home.prev,
      autostop:0, 
      next:home.next,
      before: function(curr, next, opts, fwd){
        var sign = '-',
        $info = $(next).find('.info');
        $info.css({ 'position' : 'absolute', 'left' : sign+'150px', 'opacity' : 0 });          
        $info.delay(500).animate({
          left: 0,
          opacity : 1
        }, 600, 'swing', null);   
      },
      after: function(curr, next, opts, fwd){ 
          home.prev.find('.imgs').cycle('next');
          home.next.find('.imgs').cycle('next'); 
      }
    };
     
    home.main.cycle(opts); 
    opts.before = null;
    opts.after = null; 

    opts.timeout = 0; 
    opts.startingSlide = home.main.find('.slide').length - 1;

      home.prev.find('.imgs').cycle(opts).cycle('pause');

      opts.startingSlide=1;

      home.next.find('.imgs').cycle(opts).cycle('pause'); 

      
    $('.w-banner .s-controllers').find('.prev,.next').hover(
      function(){
        $(this).animate({width:'140px'},300)
      }, 
      function(){ 
        $(this).animate({width:'34px'},500)
      }
    );
  }


  $(".w-sociales .banner").cycle({
    fx:     'scrollLeft',
    speed:  'medium',
    timeout: 0 
  });

  $(".w-sociales .block_sociales a").each(function(i,e){
    $(e).click(function(ev){
      ev.preventDefault()
      $(".w-sociales .banner").cycle(i);   
    })
  });


  $(".w-testimonios.w-explain .media,.w-testimonios.w-explain .detail  ").each(function(){
    $(this).click(function(){
      $(".w-testimonios .w-item").removeClass("explain");
      $(this).parent().addClass("explain");
    });
  });
    

  $(".w-testimonios .w-item .arrow_expand").each(function(){
    $(this).click(function(e){
      e.preventDefault(); 
      $(this).parent().removeClass("explain");
    });
  });


  $(".bt-agregar").click(function(e){
    e.preventDefault(); 
    var tbl = $(this).parents(".cnt-dfam").find("table");
    var count = tbl.find("tr").length;  
    var appendTR;
    if($(this).hasClass("bt-agregar-1")){
      var appendTR = '<tr><td><input id="parentesco_'+count+'" class="input1" type="text" name="parentesco_'+ count +'"/></td><td><input id="penregular_'+ count +'" class="input2" type="text" name="penregular_'+ count +'"/></td><td><input id="beca_'+ count +'" class="input3" type="text" name="beca_'+ count +'"/></td><td><div class="row3 r-3"><label>Motivo</label><select id="motivo_'+ count +'" class="select" name="motivo_'+ count +'"><option value="">(Seleccionar)</option><option value="A">Acad�mico</option><option value="E">Econ�mico</option><option value="H">Hijo de trabajador</option></select></div></td></tr>'
    }else if($(this).hasClass("bt-agregar-2")){
      var appendTR = '<tr><td><div class="row4 r-5"><label>Tipo</label><select id="cb_tipo_'+count+'" name="cb_tipo_'+count+'" class="select"><option value="">(Seleccionar)</option><option value="004">Asia</option><option value="005">Australia</option><option value="002">Europa</option><option value="003">Latino America</option><option value="001">Norte America</option></select></div></td><td><select id="cb_nro_'+count+'" name="cb_nro_'+count+'"class="select2"><option value="0">0</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option></select></td><td><input id="inversion_'+count+'" class="input2" type="text" name="inversion_'+count+'"/></td></tr>'
    }
    tbl.append(appendTR);
  });
  $(".bt-quitar").click(function(e){
    e.preventDefault();
    var count = $(this).parents(".cnt-dfam").find("table").find("tr").length - 1;
    var tbl = $(this).parents(".cnt-dfam").find("table");           
    if(count <= "1"){
      return false;
    }else{
      tbl.find("tr:eq("+ count +")").remove();
    }
  });


  if($.fn.fancybox){           
    $("a.fancybox").fancybox({
      'speedIn'       :   600, 
      'speedOut'      :   200, 
      'overlayShow'   :   false
    }); 
    
    $("a.lightbox").fancybox(); 

    $("a.fancyboxVideo").click(function(){
      $.fancybox({
        'padding'       : 0,
        'autoScale'     : false,
        'transitionIn'  : 'none',
        'transitionOut' : 'none',
        'width'         : 680,
        'height'        : 495,
        'href'          : this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
        'type'          : 'swf',
        'swf'           : {
           'wmode'        : 'transparent',
          'allowfullscreen'   : 'true'
        }
      });  
      return false;
    }); 
    
  }
var arrows = $('.w-intersado_up .flecha');
var arrowsFx = function(){
    arrows.hide().each(function(i,e){
      setTimeout(function(){
        $(e).show('fast');
      },500*(i+1))
    }); 
};
arrowsFx();
setInterval(arrowsFx,5000);
  
$.fn.splitUp=function(splitBy,wrapper){
	$all= $(this).find('>*');
	var fragment=Math.ceil($all.length/splitBy);
	for (i=0;i<fragment;i++)
	$all.slice(splitBy*i,splitBy*(i+1)).wrapAll(wrapper);
	return $(this);
} 
  
 $(".block_list_last").each(function(){
	var yt_url = "http://gdata.youtube.com/feeds/api/users/upacificoup/uploads?v=2&alt=json&max-results=12&start-index=1";
	$.ajax({
		type: "GET",
		url: yt_url,
		dataType:"jsonp",
		success: function(response){	
			
			if(response.feed)	{
				var htmlvideolist = [];
				var htmlvideobanner = [];					
				$(".block_list_last .list:first ul").empty();					
				$.each(response.feed.entry, function(i,data){
					var video_id=data.id;
					var video_title=data.title.$t;
					var video_img = data.media$group.media$thumbnail[0].url;	
					var video_url = data.link[0].href;					
					var itemlist = '<li><span class="media"><a href="'+ video_url+'" target="_blank"><img alt="sociales" src="'+ video_img +'" width="54" height="31"></a></span><span class="detail"><p><a href="'+ video_url+'" target="_blank">' + video_title.substr(0,60) + '</a></p></span></li>';					
					htmlvideolist.push(itemlist);		
				});
				var joinVideo = htmlvideolist.join('');				
				$(".block_list_last .list:first ul").append(joinVideo).splitUp(4, '<div class="listgroup" />');					
			}
			else
			{
				$("#result").html("<div id='no'>No Video</div>");
			}
		}
	});
}); 

  

  
  
  
  
});