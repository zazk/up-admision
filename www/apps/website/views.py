# Create your views here.
from django.shortcuts import get_object_or_404, render_to_response as render
from django.template import RequestContext
from django.template.loader import render_to_string
from django.core.files import File
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
import re
import os

# Nueva Vista
def url_template(request, template=''):
    home = -False 
    page = False
    templates = os.listdir('templates/website')
    base = set(['header.html', 'base.html', 'footer.html','aside.html','slider.html','aside-dudas',
        'index.html','base-epu.html','base-ex.html','base-mod.html','base-pensiones.html',
        'aside-agenda.html','aside-carreras.html','aside-dudas.html','aside-epu.html','aside-ex.html',
        'aside-mod.html','aside-noti.html','aside-pensiones.html','aside-testimonios.html',
        'aside-descargas.html','aside-nivelatorio.html'
        ])  
    carreras = {
        'marketing': {'title':'Marketing','class':'mode-1'},  
        'administracion': {'title':'Administracion','class':'mode-2'},  
        'contabilidad': {'title':'Contabilidad','class':'mode-3'},  
        'derecho': {'title':'Derecho','class':'mode-4'},  
        'economia': {'title':'Economia','class':'mode-5'},  
        'ing-informacion': {'title':'Ingenieria de la Informacion','class':'mode-6'},  
        'ing-empresarial': {'title':'Ingenieria Empresarial','class':'mode-7'},  
        'negocios': {'title':'Negocios Internacionales','class':'mode-8'},   
        }  
    popup = True if 'ingresos_popup' == template else False;  
    #print "TEMPLATE ___________________________________________"
    #print template
    #print "POPUP ___________________________________________"
    #print popup
    tpl = template.replace('_text','')
    if tpl in carreras and not (carreras[tpl] is None):
        page = carreras[tpl]

    print tpl




    templates = [a for a in templates if a not in base] 
    if template == 'home' :
        home = True
    if not template: 
        home = True
        template = 'index'
 

    #r = render_to_string('website/'+template+ '.html',locals() )
    #print "_____________________R OBJECT_____________________"
    #print r 

    
    #for t in templates:
        #default_storage.save('published/'+t+'.html', ContentFile( r ) )
    
    #print "_______________________END R___________________"

    return render('website/'+template+ '.html', locals(),
        context_instance=RequestContext(request))        
 